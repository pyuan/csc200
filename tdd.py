from unittest import TestCase

from polynomial import Polynomial

class PolynomialTestSuite(TestCase):

    def test_create_polynomial(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.assertEqual(self.p1, '2x^2 + 2x + 2')
