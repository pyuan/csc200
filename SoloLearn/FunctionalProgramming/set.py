peter_likes = set(["i3","bspwm","notebooks","latex word processing","polybar"])
adrian_likes = set(["i3","polybar","reddit","gmmk with pink keycaps"])

print("Adrian and Peter both like", peter_likes & adrian_likes)
print("Adrian and Peter combined like", peter_likes | adrian_likes)
