import itertools 

metals = ["H","Li","Be","Na","Mg"]
gases = ["B","C","N","O","F","Al","Si","P","S","Cl","Ar"]

ionic_bonds = list(itertools.product(metals, gases))

print(ionic_bonds)
print("There are ", len(ionic_bonds), "possible ionic bonds in the first three periods of the periodic table")
