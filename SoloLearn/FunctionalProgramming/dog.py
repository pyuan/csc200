class Dog:

    def __init__(self, name="dog", age="0"):
        self.name = name
        self.age = age

    def say_hi(self):
        print(f"Hi My Name is {self.name}")
