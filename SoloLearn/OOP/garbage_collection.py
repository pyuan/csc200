a = "garbage" # object created 
b = a + " smells" # refrence count + 1


b = "flowers smell better" #refrence count - 1
print(a)    #a stil exists because during its creation it has a reference count
            # of 1 
del a       # reference count - 1 and leads to deletion 
print(a)    # doesn't print anything because a has been deleted
