import math

class Triangle_angle:
    def __init__(self, a, b, C):        # defining stuff
        self.a  = a
        self.b = b
        self.C = C
    
    def calculate_c(self):              # law of cosines
        return math.sqrt(self.a ** 2 + self.b ** 2 - 2 * self.a * self.b *
                math.cos(math.radians(self.C)))

    # still not sure why classmethods are used. this seems redundant.
    # I'm probably missing something about the cls
    @classmethod                        
    def new_triangle(cls, a, b, C):
        return cls(a, b, C)

triangle = Triangle_angle.new_triangle(10, 11, 108)
print(triangle.calculate_c())
