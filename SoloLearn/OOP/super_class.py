class A:
    def spam(self):
        print(1)
class B(A):
    def spam(self):
        print(self)
        super().spam() # B.A.spam()

# this returns 2 because B is first called. then as B finsihes it reaches
# super().spam() which calls its parent function.
B().spam()

# Because I caled B first, self is already given as a parameter, so A.spam()
# needs an argument because I never called A itself.

# this returns nothing
A.spam

# the parameter doesn't matter, it will always print 1
A.spam(self)

 
