class Orange:
    __orangecolor = "orange"
    def printcolor(self):
        print(self.__orangecolor)

o = Orange()
o.printcolor()
#print(o.Orange__orangecolor)       # these don't work because they are hidden
#print(o.__orangecolor)

