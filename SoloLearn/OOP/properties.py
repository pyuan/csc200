class Taco:

    def __init__(self, shell, meat):
        self.shell = shell
        self.meat = meat

    @property
    def fake_meat(self):
        return False

    @fake_meat.setter
    def fake_meat(self, value):
        if value:
            taste = input("does it taste like real meat?")
            if taste.floor == "yes":
                self._fake_meat = meat
            else:
                raise ValueError("Yuck!")

taco = Taco("soft shell", "pastor")
print(taco.fake_meat)
taco.fake_meat = True
print(taco.fake_meat)
